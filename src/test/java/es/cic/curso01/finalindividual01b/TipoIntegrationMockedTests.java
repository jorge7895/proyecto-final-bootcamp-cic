package es.cic.curso01.finalindividual01b;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import es.cic.curso01.finalindividual01b.controllers.TipoController;
import es.cic.curso01.finalindividual01b.models.Tipo;
import es.cic.curso01.finalindividual01b.services.TipoService;

@SpringBootTest
class TipoIntegrationMockedTests {

	@MockBean
	private TipoService tipoService;

	@Autowired
	private TipoController tipoController;

	@Test
	void testCreate() {

		Tipo tipo = new Tipo();
		tipo.setDescripcion("Descripción de test");
		tipo.setNombre("Tipo de test");
		tipo.setId(1);

		when(tipoService.crearTiponuevo(tipo)).thenReturn(tipo);

		assertEquals(ResponseEntity.status(HttpStatus.CREATED)
				.contentType(MediaType.APPLICATION_JSON).body(tipo),
				tipoController.crearTipo(tipo));

		verify(tipoService, times(1)).crearTiponuevo(tipo);

	}

}
