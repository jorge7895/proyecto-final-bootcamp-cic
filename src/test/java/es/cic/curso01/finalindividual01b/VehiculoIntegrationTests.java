package es.cic.curso01.finalindividual01b;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import es.cic.curso01.finalindividual01b.models.Marca;
import es.cic.curso01.finalindividual01b.models.Tipo;
import es.cic.curso01.finalindividual01b.models.Vehiculo;
import es.cic.curso01.finalindividual01b.utils.TipoTraccion;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
class VehiculoIntegrationTests {

	@Autowired
	private MockMvc mvc;

	@Autowired
	private ObjectMapper objectMapper;
	
	@PersistenceContext
	private EntityManager em;
	
	private Vehiculo vehiculo;
	
	@BeforeEach
	void setUp() throws Exception {
		
		Tipo tipo = new Tipo();
		tipo.setNombre("Turismo");
		tipo.setDescripcion("El automovil clasico de uso diario");
		em.persist(tipo);
		
		Marca marca = new Marca();
		marca.setNombre("VW");
		marca.setSede("Alemania");
		marca.setAnoCreacion(1820);
		marca.setDescripcion("VolksWagen");
		em.persist(marca);
		
		vehiculo = new Vehiculo();
		vehiculo.setModelo("Polo");
		vehiculo.setMarca(marca);
		vehiculo.setAnoFabricacion(2000);
		vehiculo.setPlazas(5);
		vehiculo.setTipo(tipo);
		vehiculo.setTraccion(TipoTraccion.DELANTERA);
		em.persist(vehiculo);
	}
	
	@Test
	void testCrearVehiculo() throws JsonProcessingException, Exception {
		
		vehiculo.setModelo("Golf");
		
		mvc.perform(post("/api/v1/vehiculo")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(vehiculo)))
				.andExpect(status().is2xxSuccessful())
				.andExpect(jsonPath("$.modelo",is("Golf")))
				.andDo(print());
	}
	
	@Test
	void testObtenerVehiculos() throws JsonProcessingException, Exception {
		
		mvc.perform(get("/api/v1/vehiculo"))				
				.andExpect(status().is2xxSuccessful())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$[0].modelo",is("Polo")))
				.andDo(print());
	}
	
	@Test
	void testObtenerVehiculoPorId() throws JsonProcessingException, Exception {
		
		String id = String.valueOf(vehiculo.getId());
		
		mvc.perform(get("/api/v1/vehiculo/id")
		.param("id",id))
				.andExpect(status().is2xxSuccessful())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.modelo",is("Polo")))
				.andDo(print());
	}
	
	@Test
	void testObtenerVehiculoPorMarca() throws JsonProcessingException, Exception {
		
		String marca = "VW";
		
		mvc.perform(get("/api/v1/vehiculo/marca")
		.param("marca",marca))
				.andExpect(status().is2xxSuccessful())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$[0].modelo",is("Polo")))
				.andDo(print());
	}
	
	@Test
	void testObtenerVehiculoPorTipo() throws JsonProcessingException, Exception {
		
		String tipo = "Turismo";
		
		mvc.perform(get("/api/v1/vehiculo/tipo")
		.param("tipo",tipo))
				.andExpect(status().is2xxSuccessful())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$[0].modelo",is("Polo")))
				.andDo(print());
	}
	
	@Test
	void testObtenerVehiculoPorModelo() throws JsonProcessingException, Exception {
		
		String modelo = "Polo";
		
		mvc.perform(get("/api/v1/vehiculo/modelo")
		.param("modelo",modelo))
				.andExpect(status().is2xxSuccessful())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.modelo",is("Polo")))
				.andDo(print());
	}
	
	@Test
	void testEliminarVehiculo() throws JsonProcessingException, Exception {
				
		String id = String.valueOf(vehiculo.getId());
		
		mvc.perform(delete("/api/v1/vehiculo/eliminar")
				.param("id",id))
				.andExpect(status().is2xxSuccessful())
				.andDo(print());
	}
	
	@Test
	void testModificarVehiculo() throws JsonProcessingException, Exception {
		
		vehiculo.setModelo("Golf");
		String id = String.valueOf(vehiculo.getId());
		
		
		mvc.perform(put("/api/v1/vehiculo/modificar")
				.param("id",id)
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(vehiculo)))
				.andExpect(status().is2xxSuccessful())
				.andExpect(jsonPath("$.modelo",is("Golf")))
				.andDo(print());
	}

}
