package es.cic.curso01.finalindividual01b;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import es.cic.curso01.finalindividual01b.models.Grupo;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
class GrupoIntegrationTests {

	@Autowired
	private MockMvc mvc;

	@Autowired
	private ObjectMapper objectMapper;
	
	@PersistenceContext
	private EntityManager em;
	
	private Grupo grupo;
	
	@BeforeEach
	void setUp() throws Exception {
		
		grupo = new Grupo();
		grupo.setNombre("Grupo 1");
		grupo.setDescripcion("Prueba de grupo");
		em.persist(grupo);
	}
	
	@Test
	void testCrearGrupo() throws JsonProcessingException, Exception {
		
		grupo.setNombre("Grupo 2");
		
		mvc.perform(post("/api/v1/grupo")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(grupo)))
				.andExpect(status().is2xxSuccessful())
				.andExpect(jsonPath("$.nombre",is("Grupo 2")))
				.andDo(print());
	}
	
	@Test
	void testObtenerGrupos() throws JsonProcessingException, Exception {
		
		mvc.perform(get("/api/v1/grupo"))				
				.andExpect(status().is2xxSuccessful())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.[0].nombre",is("Grupo 1")))
				.andDo(print());
	}
	
	@Test
	void testObtenerGruposPorId() throws JsonProcessingException, Exception {
		
		String id = String.valueOf(grupo.getId());
		
		mvc.perform(get("/api/v1/grupo/id")
		.param("id",id))
				.andExpect(status().is2xxSuccessful())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.nombre",is("Grupo 1")))
				.andDo(print());
	}
	
	@Test
	void testEliminarGrupo() throws JsonProcessingException, Exception {
				
		String id = String.valueOf(grupo.getId());
		
		mvc.perform(delete("/api/v1/grupo/eliminar")
				.param("id",id))
				.andExpect(status().is2xxSuccessful())
				.andDo(print());
	}
	
	@Test
	void testModificarGrupopo() throws JsonProcessingException, Exception {
		
		grupo.setNombre("Grupo 2");
		String id = String.valueOf(grupo.getId());
		
		
		mvc.perform(put("/api/v1/grupo/modificar")
				.param("id",id)
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(grupo)))
				.andExpect(status().is2xxSuccessful())
				.andExpect(jsonPath("$.nombre",is("Grupo 2")))
				.andDo(print());
	}

}
