package es.cic.curso01.finalindividual01b;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import es.cic.curso01.finalindividual01b.models.Tipo;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
class TipoIntegrationTests {

	@Autowired
	private MockMvc mvc;

	@Autowired
	private ObjectMapper objectMapper;
	
	@PersistenceContext
	private EntityManager em;
	
	private Tipo tipo;
	
	@BeforeEach
	void setUp() throws Exception {
		
		tipo = new Tipo();
		tipo.setNombre("Turismo");
		tipo.setDescripcion("El automovil clasico de uso diario");
		em.persist(tipo);
	}
	
	@Test
	void testCrearTipo() throws JsonProcessingException, Exception {
		
		tipo.setNombre("Motocicleta");
		
		mvc.perform(post("/api/v1/tipo")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(tipo)))
				.andExpect(status().is2xxSuccessful())
				.andExpect(jsonPath("$.nombre",is("Motocicleta")))
				.andDo(print());
	}
	
	@Test
	void testObtenerTipos() throws JsonProcessingException, Exception {
		
		mvc.perform(get("/api/v1/tipo"))				
				.andExpect(status().is2xxSuccessful())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$[0].nombre",is("Turismo")))
				.andDo(print());
	}
	
	@Test
	void testObtenerTiposPorId() throws JsonProcessingException, Exception {
		
		String id = String.valueOf(tipo.getId());
		
		mvc.perform(get("/api/v1/tipo/id")
		.param("id",id))
				.andExpect(status().is2xxSuccessful())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.nombre",is("Turismo")))
				.andDo(print());
	}
	
	@Test
	void testObtenerVehiculoPorNombre() throws JsonProcessingException, Exception {
		
		String nombre = "Turismo";
		
		mvc.perform(get("/api/v1/tipo/nombre")
		.param("nombre",nombre))
				.andExpect(status().is2xxSuccessful())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.nombre",is("Turismo")))
				.andDo(print());
	}
	
	@Test
	void testEliminarTipo() throws JsonProcessingException, Exception {
				
		String id = String.valueOf(tipo.getId());
		
		mvc.perform(delete("/api/v1/tipo/eliminar")
				.param("id",id))
				.andExpect(status().is2xxSuccessful())
				.andDo(print());
	}
	
	@Test
	void testModificarTipo() throws JsonProcessingException, Exception {
		
		tipo.setNombre("Motocicleta");
		String id = String.valueOf(tipo.getId());
		
		
		mvc.perform(put("/api/v1/tipo/modificar")
				.param("id",id)
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(tipo)))
				.andExpect(status().is2xxSuccessful())
				.andExpect(jsonPath("$.nombre",is("Motocicleta")))
				.andDo(print());
	}
	
	@Test
	void testModificarTipoNoExistente() throws JsonProcessingException, Exception {
		
		tipo.setNombre("Motocicleta");
		String id = "9999";
		
		
		mvc.perform(put("/api/v1/tipo/modificar")
				.param("id",id)
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(tipo)))
				.andExpect(status().is4xxClientError())
				.andDo(print());
	}

}
