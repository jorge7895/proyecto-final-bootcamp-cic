package es.cic.curso01.finalindividual01b;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import es.cic.curso01.finalindividual01b.models.Marca;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
class MarcaIntegrationTests {

	@Autowired
	private MockMvc mvc;

	@Autowired
	private ObjectMapper objectMapper;
	
	@PersistenceContext
	private EntityManager em;
	
	private Marca marca;
	
	@BeforeEach
	void setUp() throws Exception {
		
		marca = new Marca();
		marca.setNombre("VW");
		marca.setSede("Alemania");
		marca.setAnoCreacion(1820);
		marca.setDescripcion("VolksWagen");
		em.persist(marca);
				
	}
	
	@Test
	void testCrearMarca() throws JsonProcessingException, Exception {
		
		marca.setNombre("Seat");
		
		mvc.perform(post("/api/v1/marca")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(marca)))
				.andExpect(status().is2xxSuccessful())
				.andExpect(jsonPath("$.nombre",is("Seat")))
				.andDo(print());
	}
	
	@Test
	void testObtenerMarcas() throws JsonProcessingException, Exception {
		
		mvc.perform(get("/api/v1/marca"))				
				.andExpect(status().is2xxSuccessful())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$[0].nombre",is("VW")))
				.andDo(print());
	}
	
	@Test
	void testObtenerMarcasPorId() throws JsonProcessingException, Exception {
		
		String id = String.valueOf(marca.getId());
		
		mvc.perform(get("/api/v1/marca/id")
		.param("id",id))
				.andExpect(status().is2xxSuccessful())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.nombre",is("VW")))
				.andDo(print());
	}
	
	@Test
	void testEliminarMarca() throws JsonProcessingException, Exception {
				
		String id = String.valueOf(marca.getId());
		
		mvc.perform(delete("/api/v1/marca/eliminar")
				.param("id",id))
				.andExpect(status().is2xxSuccessful())
				.andDo(print());
	}
	
	@Test
	void testModificarMarca() throws JsonProcessingException, Exception {
		
		marca.setNombre("Seat");
		String id = String.valueOf(marca.getId());
		
		
		mvc.perform(put("/api/v1/marca/modificar")
				.param("id",id)
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(marca)))
				.andExpect(status().is2xxSuccessful())
				.andExpect(jsonPath("$.nombre",is("Seat")))
				.andDo(print());
	}

}
