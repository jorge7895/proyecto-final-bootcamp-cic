--  *********************************************************************
--  Update Database Script
--  *********************************************************************
--  Change Log: vehiculos.xml
--  Ran at: 9/30/22, 11:51 AM
--  Against: u_apuestas@offline:mysql
--  Liquibase version: 4.14.0
--  *********************************************************************

--  Changeset vehiculos.xml::1664531466199-1::curso01 (generated)
CREATE TABLE grupo (id BIGINT AUTO_INCREMENT NOT NULL, descripcion VARCHAR(255) NULL, nombre VARCHAR(255) NULL, version BIGINT NOT NULL, CONSTRAINT PK_GRUPO PRIMARY KEY (id));

--  Changeset vehiculos.xml::1664531466199-2::curso01 (generated)
CREATE TABLE grupo_vehiculo (id BIGINT AUTO_INCREMENT NOT NULL, grupo_id BIGINT NULL, vehiculo_id BIGINT NULL, CONSTRAINT PK_GRUPO_VEHICULO PRIMARY KEY (id));

--  Changeset vehiculos.xml::1664531466199-3::curso01 (generated)
CREATE TABLE marca (id BIGINT AUTO_INCREMENT NOT NULL, ano_creacion INT NOT NULL, descripcion VARCHAR(255) NULL, nombre VARCHAR(255) NULL, sede VARCHAR(255) NULL, version BIGINT NOT NULL, CONSTRAINT PK_MARCA PRIMARY KEY (id));

--  Changeset vehiculos.xml::1664531466199-4::curso01 (generated)
CREATE TABLE tipo (id BIGINT AUTO_INCREMENT NOT NULL, descripcion VARCHAR(255) NULL, nombre VARCHAR(255) NULL, version BIGINT NOT NULL, CONSTRAINT PK_TIPO PRIMARY KEY (id));

--  Changeset vehiculos.xml::1664531466199-5::curso01 (generated)
CREATE TABLE vehiculo (id BIGINT AUTO_INCREMENT NOT NULL, ano_fabricacion INT NOT NULL, combustible INT NULL, descripcion VARCHAR(255) NULL, kilometros DOUBLE NOT NULL, matricula VARCHAR(255) NULL, modelo VARCHAR(255) NULL, plazas INT NOT NULL, puertas INT NOT NULL, traccion INT NULL, version BIGINT NOT NULL, marca_id BIGINT NULL, tipo_id BIGINT NULL, CONSTRAINT PK_VEHICULO PRIMARY KEY (id));

--  Changeset vehiculos.xml::1664531466199-6::curso01 (generated)
CREATE INDEX fk_marca_marcaId ON vehiculo(marca_id);

--  Changeset vehiculos.xml::1664531466199-7::curso01 (generated)
CREATE INDEX fk_tipo_tipoId ON vehiculo(tipo_id);

--  Changeset vehiculos.xml::1664531466199-8::curso01 (generated)
CREATE INDEX grupo_id_fk ON grupo_vehiculo(grupo_id);

--  Changeset vehiculos.xml::1664531466199-9::curso01 (generated)
CREATE INDEX vehiculo_id_fk ON grupo_vehiculo(vehiculo_id);

