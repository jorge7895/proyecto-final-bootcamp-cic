CREATE DATABASE IF NOT EXISTS vehiculos;

use vehiculos;

CREATE TABLE IF NOT EXISTS grupo (
  id BIGINT(20) NOT NULL AUTO_INCREMENT,
  descripcion VARCHAR(255) NULL DEFAULT NULL,
  nombre VARCHAR(255) NULL DEFAULT NULL,
  version BIGINT(20) NOT NULL,
  PRIMARY KEY (id))
ENGINE = MyISAM
AUTO_INCREMENT = 21
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS grupo_vehiculo (
  id BIGINT(20) NOT NULL AUTO_INCREMENT,
  grupo_id BIGINT(20) NULL DEFAULT NULL,
  vehiculo_id BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (id),
  INDEX grupo_id_fk (grupo_id ASC),
  INDEX vehiculo_id_fk (vehiculo_id ASC))
ENGINE = MyISAM
AUTO_INCREMENT = 10
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS marca (
  id BIGINT(20) NOT NULL AUTO_INCREMENT,
  ano_creacion INT(11) NOT NULL,
  descripcion VARCHAR(255) NULL DEFAULT NULL,
  nombre VARCHAR(255) NULL DEFAULT NULL,
  sede VARCHAR(255) NULL DEFAULT NULL,
  version BIGINT(20) NOT NULL,
  PRIMARY KEY (id))
ENGINE = MyISAM
AUTO_INCREMENT = 11
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS tipo (
  id BIGINT(20) NOT NULL AUTO_INCREMENT,
  descripcion VARCHAR(255) NULL DEFAULT NULL,
  nombre VARCHAR(255) NULL DEFAULT NULL,
  version BIGINT(20) NOT NULL,
  PRIMARY KEY (id))
ENGINE = MyISAM
AUTO_INCREMENT = 8
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS vehiculo (
  id BIGINT(20) NOT NULL AUTO_INCREMENT,
  ano_fabricacion INT(11) NOT NULL,
  combustible INT(11) NULL DEFAULT NULL,
  descripcion VARCHAR(255) NULL DEFAULT NULL,
  kilometros DOUBLE NOT NULL,
  matricula VARCHAR(255) NULL DEFAULT NULL,
  modelo VARCHAR(255) NULL DEFAULT NULL,
  plazas INT(11) NOT NULL,
  puertas INT(11) NOT NULL,
  traccion INT(11) NULL DEFAULT NULL,
  version BIGINT(20) NOT NULL,
  marca_id BIGINT(20) NULL DEFAULT NULL,
  tipo_id BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (id),
  INDEX fk_marca_marcaId (marca_id ASC),
  INDEX fk_tipo_tipoId (tipo_id ASC))
ENGINE = MyISAM
AUTO_INCREMENT = 11
DEFAULT CHARACTER SET = utf8;