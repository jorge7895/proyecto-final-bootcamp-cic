insert into grupo (id, nombre, descripcion, version) 
values 
(1, 'Furgonetas', 'Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet.', 1),
(2, 'Trabajo', 'Vestibulum sed magna at nunc commodo placerat. Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus.', 2),
(3, 'Cotidiano', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo.', 2),
(4, 'Utilitario', 'Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl. Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.', 1),
(5, 'Disfrute', 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl. Aenean lectus. Pellentesque eget nunc.', 2),
(6, 'Motocicletas', 'Integer ac neque. Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus. In sagittis dui vel nisl.', 1),
(7, 'Descapotables', 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.', 2);

insert into tipo (id, nombre, descripcion, version) 
values 
(1, 'Ciclomotor', 'Vehículo de dos o tres ruedas provisto de un motor de cilindrada no superior a 50 cm3.', 1),
(2, 'Turismo', 'Automóvil distinto de la motocicleta, especialmente concebido y construido para el transporte de personas y con capacidad de hasta 9 plazas.', 2),
(3, 'Furgón/Furgoneta', 'Automóvil destinado al transporte de mercancías cuya cabina está integrada en el resto de la carrocería con masa máxima autorizada igual o inferior a 3.500 kg.', 2),
(4, 'Derivado de turismo', 'Vehículo automóvil destinado a servicios o a transporte exclusivo de mercancías, derivado de un turismo del cual conserva la carrocería y dispone únicamente de una fila de asientos.', 1),
(5, 'Vehículo mixto adaptable', 'Automóvil especialmente dispuesto para el transporte, simultáneo o no, de mercancías y personas hasta un máximo de 9 incluido el conductor.', 2),
(6, 'Motocicleta', 'Automóvil de dos ruedas o con sidecar.', 1),
(7, 'Bicicleta', 'Ciclo de dos ruedas.', 2);

insert into marca (id, ano_creacion, descripcion, version, sede, nombre) 
values 
(1, 2011, 'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. Vestibulum ac est lacinia nisi venenatis tristique.', 2, 'Portugal', 'Ford'),
(2, 2011, 'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.', 2, 'Mozambique', 'Dodge'),
(3, 1965, 'Nullam varius. Nulla facilisi. Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit.', 2, 'Indonesia', 'Ford'),
(4, 1998, 'Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi. Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla.', 1, 'China', 'Lincoln'),
(5, 1998, 'Quisque ut erat. Curabitur gravida nisi at nibh. In hac habitasse platea dictumst.', 1, 'Venezuela', 'Nissan'),
(6, 2009, 'Suspendisse potenti. Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum.', 2, 'Peru', 'Honda'),
(7, 2009, 'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilias.', 1, 'Kenya', 'Nissan'),
(8, 1997, 'Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus. Curabitur at ipsum ac tellus semper interdum.', 1, 'Brazil', 'Nissan'),
(9, 1984, 'Mauris sit amet eros. Suspendisse accumsan tortor quis turpis. Sed ante. Vivamus tortor.', 1, 'France', 'Mitsubishi'),
(10, 1992, 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius. Integer ac leo.', 2, 'Russia', 'GMC');

insert into vehiculo (id, ano_fabricacion, combustible, descripcion, kilometros, matricula, modelo, plazas, puertas, traccion, version, marca_id, tipo_id) 
values 
(1, 2005, 1, 'Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem. ', 1, '1426WJW', 'E-Class', 3, 1, 1, 2, 10, 3),
(2, 1986, 1, 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus.', 2, '2597QTO', 'Astra', 2, 4, 2, 3, 4, 1),
(3, 2004, 2, 'Proin at turpis a pede posuere nonummy. Integer non velit. ', 3, '0204AKJ', 'RSX', 4, 2, 'DELANTERA', 1, 10, 1),
(4, 1992, 3, 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in.', 4, '6473TNG', 'S-Class', 2, 2, 1, 1, 3, 4),
(5, 1993, 2, 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', 5, '4781MKR', 'E-Series', 1, 5, 2, 2, 5, 4),
(6, 1983, 1, 'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus.', 6, '3072SCP', '626', 5, 4, 2, 2, 1, 3),
(7, 1997, 2, 'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat. Curabitur gravida nisi at nibh.', 7, '5035KVP', 'Mentor', 4, 5, 1, 3, 9, 7),
(8, 1994, 2, 'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum .', 8, '1494WIK', 'Cutlass Cruiser', 2, 4, 1, 2, 6, 1),
(9, 1987, 1, 'Ut tellus. Nulla ut erat id mauris vulputate elementum. Nullam varius.', 9, '9334RBQ', 'Brat', 1, 1, 2, 3, 7, 7),
(10, 2013, 3, 'Etiam justo. Etiam pretium iaculis justo. In hac habitasse platea dictumst. Etiam faucibus cursus urna.', 10, '4581SPG', 'Cruze', 2, 2, 1, 1, 10, 1);