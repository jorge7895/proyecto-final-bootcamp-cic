package es.cic.curso01.finalindividual01b;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FinalIndividual01BApplication {

	public static void main(String[] args) {
		SpringApplication.run(FinalIndividual01BApplication.class, args);
	}

}
