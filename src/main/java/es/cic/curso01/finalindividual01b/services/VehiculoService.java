package es.cic.curso01.finalindividual01b.services;

import java.util.Optional;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import es.cic.curso01.finalindividual01b.exceptions.TipoException;
import es.cic.curso01.finalindividual01b.models.Vehiculo;
import es.cic.curso01.finalindividual01b.repositories.IVehiculoRepository;

@Service
@Transactional
public class VehiculoService {

	private static final Logger LOGGER = LogManager.getLogger(VehiculoService.class);
	
	@Autowired
	private IVehiculoRepository vehiculoRepository;
	
	public Page<Vehiculo> obtenerTodosLosVehiculos(Pageable pageable){
		LOGGER.trace("Accediendo a la lectura de vehiculos");
		
		return vehiculoRepository.findAll(pageable);
	}
	
	public Page<Vehiculo> obtenerTodosLosVehiculosDeUnaMarca(Pageable pageable, String marca){
		LOGGER.trace("Accediendo a la lectura de vehiculos por marca");
		
		return vehiculoRepository.findAllByMarca(marca, pageable);
	}
	
	public Page<Vehiculo> obtenerTodosLosVehiculosDeUnTipo(Pageable pageable, String tipo){
		LOGGER.trace("Accediendo a la lectura de vehiculos por tipo");
		
		return vehiculoRepository.findAllByTipo(tipo, pageable);
	}
	
	public Optional<Vehiculo> obtenerVehiculoPorId(long id) {
		LOGGER.trace("Accediendo a la lectura de vehiculos por id");
		
		return vehiculoRepository.findById(id);
	}
	
	public Optional<Vehiculo> obtenerVehiculoPorModelo(String modelo) {
		LOGGER.trace("Accediendo a la lectura de tipos por modelo");
		
		return vehiculoRepository.findByModelo(modelo);
	}
	
	public Vehiculo crearVehiculonuevo(Vehiculo vehiculo) {
		LOGGER.trace("Creando un vehiculo nuevo");
		
		return vehiculoRepository.save(vehiculo);
	}
	
	public Vehiculo actualizarElVehiculo(long id, Vehiculo vehiculo) {
		LOGGER.trace("Actualizado el vehiculo");
		
		Optional<Vehiculo> vehiculoParaActualizar = vehiculoRepository.findById(id);
		
		if(vehiculoParaActualizar.isEmpty()) {
			throw new TipoException("No se encontro ningún vehiculo para editar");
		}
		
		vehiculoParaActualizar.get().setModelo(vehiculo.getModelo());
		vehiculoParaActualizar.get().setMarca(vehiculo.getMarca());
		vehiculoParaActualizar.get().setDescripcion(vehiculo.getDescripcion());
		vehiculoParaActualizar.get().setAnoFabricacion(vehiculo.getAnoFabricacion());
		vehiculoParaActualizar.get().setKilometros(vehiculo.getKilometros());
		vehiculoParaActualizar.get().setMatricula(vehiculo.getMatricula());
		vehiculoParaActualizar.get().setPlazas(vehiculo.getPlazas());
		vehiculoParaActualizar.get().setPuertas(vehiculo.getPuertas());
		vehiculoParaActualizar.get().setTraccion(vehiculo.getTraccion());
		vehiculoParaActualizar.get().setTipo(vehiculo.getTipo());
		
		return vehiculoRepository.save(vehiculoParaActualizar.get());
	}
	
	public void eliminarVehiculo(long id) {
		LOGGER.trace("Eliminando el vehiculo");
		
		vehiculoRepository.deleteById(id);
	}
	
	public long getNumPaginas(int size) {
		
		long cantidadDeVehiculos = vehiculoRepository.count();
		
		long mod = cantidadDeVehiculos % size;
		
		if (mod > 0) {
			mod = 1;
		}
		return (cantidadDeVehiculos / size) + mod;
	}
}
