package es.cic.curso01.finalindividual01b.services;

import java.util.Optional;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import es.cic.curso01.finalindividual01b.exceptions.TipoException;
import es.cic.curso01.finalindividual01b.models.Grupo;
import es.cic.curso01.finalindividual01b.repositories.IGrupoRepository;

@Service
@Transactional
public class GrupoService {
	
	private static final Logger LOGGER = LogManager.getLogger(GrupoService.class);
	
	@Autowired
	private IGrupoRepository grupoRepository;
	
	public Page<Grupo> obtenerTodosLosGrupos(Pageable pageable){
		LOGGER.trace("Accediendo a la lectura de Grupos");
		
		return grupoRepository.findAll(pageable);
	}
	
	public Optional<Grupo> obtenerGrupoPorId(long id) {
		LOGGER.trace("Accediendo a la lectura de Grupos por id");
		
		return grupoRepository.findById(id);
	}
	
	public Optional<Grupo> obtenerGrupoPorNombre(String nombre) {
		LOGGER.trace("Accediendo a la lectura de Grupos por nombre");
		
		return grupoRepository.findByNombre(nombre);
	}
	
	public Grupo crearGruponuevo(Grupo grupo) {
		LOGGER.trace("Creando un Grupo nuevo");
		
		return grupoRepository.save(grupo);
	}
	
	public Grupo actualizarElGrupo(long id, Grupo grupo) {
		LOGGER.trace("Actualizado el Grupo");
		
		Optional<Grupo> grupoParaActualizar = grupoRepository.findById(id);
		
		if(grupoParaActualizar.isEmpty()) {
			throw new TipoException("No se encontro ningún Grupo para editar");
		}
		
		grupoParaActualizar.get().setNombre(grupo.getNombre());
		grupoParaActualizar.get().setDescripcion(grupo.getDescripcion());
		
		return grupoRepository.save(grupoParaActualizar.get());
	}
	
	public void eliminarGrupo(long id) {
		LOGGER.trace("Eliminando el Grupo");
		
		grupoRepository.deleteById(id);	
	}
	
	public long getNumPaginas(int size) {
		
		long cantidadDeGrupos = grupoRepository.count();
		
		long mod = cantidadDeGrupos % size;
		
		if (mod > 0) {
			mod = 1;
		}
		return (cantidadDeGrupos / size) + mod;
	}
}
