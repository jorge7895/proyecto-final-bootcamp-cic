package es.cic.curso01.finalindividual01b.services;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import es.cic.curso01.finalindividual01b.exceptions.TipoException;
import es.cic.curso01.finalindividual01b.models.Grupo;
import es.cic.curso01.finalindividual01b.models.GrupoVehiculo;
import es.cic.curso01.finalindividual01b.models.Vehiculo;
import es.cic.curso01.finalindividual01b.repositories.IGrupoRepository;
import es.cic.curso01.finalindividual01b.repositories.IGrupoVehiculoRepository;
import es.cic.curso01.finalindividual01b.repositories.IVehiculoRepository;

@Service
@Transactional
public class GrupoVehiculoService {
	
	private static final Logger LOGGER = LogManager.getLogger(GrupoVehiculoService.class);
	
	@Autowired
	private IGrupoVehiculoRepository grupoVehiculoRepository;
	
	@Autowired
	private IGrupoRepository grupoRepository;
	
	@Autowired
	private IVehiculoRepository vehiculoRepository;
	
	public Page<GrupoVehiculo> obtenerTodosLosGrupoVehiculo(Pageable pageable){
		LOGGER.trace("Accediendo a la lectura de Grupo Vehiculo");
		
		return grupoVehiculoRepository.findAll(pageable);
	}
	
	public Page<Vehiculo> obtenerVehiculosDeGrupo(long grupo, Pageable pageable){
		LOGGER.trace("Accediendo a la lectura de Vehiculos de un grupo");
		
		Optional<Grupo> grupoBuscado = grupoRepository.findById(grupo);
		
		if(grupoBuscado.isEmpty()) {
			throw new TipoException("No se encontro el Grupo");
		}
				
		return grupoVehiculoRepository.findVehiculoByGrupo(grupoBuscado.get(), pageable);
	}
	
	public Page<Grupo> obtenerGruposDeVehiculo(long vehiculo, Pageable pageable){
		LOGGER.trace("Accediendo a la lectura de grupos de un vehiculo");
		
		Optional<Vehiculo> vehiculoBuscado = vehiculoRepository.findById(vehiculo);
		
		if(vehiculoBuscado.isEmpty()) {
			throw new TipoException("No se encontro el Vehiculo");
		}
		
		return grupoVehiculoRepository.findByGrupoByVehiculo(vehiculoBuscado.get(), pageable);
	}
	
	public Optional<GrupoVehiculo> obtenerGrupoVehiculoPorId(long id) {
		LOGGER.trace("Accediendo a la lectura de Grupo Vehiculo por id");
		
		return grupoVehiculoRepository.findById(id);
	}
	
	public List<Optional<GrupoVehiculo>> obtenerGrupoVehiculoPorGrupo(long grupo) {
		LOGGER.trace("Accediendo a la lectura de Grupo Vehiculo por grupo");
		
		Optional<Grupo> grupoBuscado = grupoRepository.findById(grupo);
		
		if(grupoBuscado.isEmpty()) {
			throw new TipoException("No se encontro el Grupo");
		}
		
		return grupoVehiculoRepository.findByGrupo(grupoBuscado.get());
	}
	
	public GrupoVehiculo crearGrupoVehiculonuevo(GrupoVehiculo grupoVehiculo) {
		LOGGER.trace("Creando un Grupo Vehiculo nuevo");
		
		return grupoVehiculoRepository.save(grupoVehiculo);
	}
	
	public List<GrupoVehiculo> crearVariosGrupoVehiculonuevo(List<GrupoVehiculo> grupoVehiculo) {
		LOGGER.trace("Creando un Grupo Vehiculo nuevo");
		
		return grupoVehiculoRepository.saveAll(grupoVehiculo);
	}
	
	public GrupoVehiculo actualizarElGrupoVehiculo(long id, GrupoVehiculo grupoVehiculo) {
		LOGGER.trace("Actualizado el Grupo Vehiculo");
		
		Optional<GrupoVehiculo> grupoVehiculoParaActualizar = grupoVehiculoRepository.findById(id);
		
		if(grupoVehiculoParaActualizar.isEmpty()) {
			throw new TipoException("No se encontro ningún Grupo Vehiculo para editar");
		}
		
		grupoVehiculoParaActualizar.get().setGrupo(grupoVehiculo.getGrupo());
		grupoVehiculoParaActualizar.get().setVehiculo(grupoVehiculo.getVehiculo());
		
		return grupoVehiculoRepository.save(grupoVehiculoParaActualizar.get());
	}
	
	public void eliminarGrupoVehiculo(long id) {
		LOGGER.trace("Eliminando el Grupo Vehiculo");
		
		if(grupoVehiculoRepository.findById(id) != null) {
			grupoVehiculoRepository.deleteById(id);
		}
	}
	
	public void eliminarVariosGrupoVehiculo(List<GrupoVehiculo> grupoVehiculo) {
		LOGGER.trace("Eliminando varios Grupo Vehiculo");
		
		grupoVehiculoRepository.deleteAll(grupoVehiculo);
	}
	
	public long getNumPaginas(int size) {
		
		long cantidadDeVehiculos = grupoVehiculoRepository.count();
		
		long mod = cantidadDeVehiculos % size;
		
		if (mod > 0) {
			mod = 1;
		}
		return (cantidadDeVehiculos / size) + mod;
	}
}
