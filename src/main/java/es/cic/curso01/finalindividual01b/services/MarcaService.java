package es.cic.curso01.finalindividual01b.services;

import java.util.Optional;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import es.cic.curso01.finalindividual01b.exceptions.TipoException;
import es.cic.curso01.finalindividual01b.models.Marca;
import es.cic.curso01.finalindividual01b.repositories.IMarcaRepository;

@Service
@Transactional
public class MarcaService {

	private static final Logger LOGGER = LogManager.getLogger(MarcaService.class);
	
	@Autowired
	private IMarcaRepository marcaRepository;
	
	public Page<Marca> obtenerTodasLasMarcas(Pageable pageable){
		LOGGER.trace("Accediendo a la lectura de marcas");
		
		return marcaRepository.findAll(pageable);
	}
	
	public Optional<Marca> obtenerMarcaPorId(long idMarca) {
		LOGGER.trace("Accediendo a la lectura de marcas por id");
		
		return marcaRepository.findById(idMarca);
	}
	
	public Optional<Marca> obtenerMarcaPorNombre(String nombre) {
		LOGGER.trace("Accediendo a la lectura de marca por nombre");
		
		return marcaRepository.findByNombre(nombre);
	}
	
	public Marca crearMarcanueva(Marca marca) {
		LOGGER.trace("Creando una marca nuevo");
		
		return marcaRepository.save(marca);
	}
	
	public Marca actualizarLaMarca(long id, Marca marca) {
		LOGGER.trace("Actualizado la marca");
		
		Optional<Marca> marcaParaActualizar = marcaRepository.findById(id);
		
		if(marcaParaActualizar.isEmpty()) {
			throw new TipoException("No se encontro ninguna marca para editar");
		}
		
		marcaParaActualizar.get().setNombre(marca.getNombre());
		marcaParaActualizar.get().setDescripcion(marca.getDescripcion());
		marcaParaActualizar.get().setAnoCreacion(marca.getAnoCreacion());
		marcaParaActualizar.get().setSede(marca.getSede());
		
		return marcaRepository.save(marcaParaActualizar.get());
	}
	
	public void eliminarMarca(long idMarca) {
		LOGGER.trace("Eliminando marca");
		
		marcaRepository.deleteById(idMarca);
	}
	
	public long getNumPaginas(int size) {
		
		long cantidadDeMarcas = marcaRepository.count();
		
		long mod = cantidadDeMarcas % size;
		
		if (mod > 0) {
			mod = 1;
		}
		return (cantidadDeMarcas / size) + mod;
	}
}
