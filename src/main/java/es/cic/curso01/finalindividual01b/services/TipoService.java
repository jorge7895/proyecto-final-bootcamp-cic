package es.cic.curso01.finalindividual01b.services;

import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.cic.curso01.finalindividual01b.exceptions.TipoException;
import es.cic.curso01.finalindividual01b.models.Tipo;
import es.cic.curso01.finalindividual01b.repositories.ITipoRepository;

@Service
@Transactional
public class TipoService {

	private static final Logger LOGGER = LogManager.getLogger(TipoService.class);
	
	@Autowired
	private ITipoRepository tipoRepository;
	
	@Transactional(readOnly = true)
	public Page<Tipo> obtenerTodosLosTipos(Pageable pageable){
		LOGGER.trace("Accediendo a la lectura de tipos");
		
		return tipoRepository.findAll(pageable);
	}
	
	@Transactional(readOnly = true)
	public Optional<Tipo> obtenerTipoPorId(long idTipo) {
		LOGGER.trace("Accediendo a la lectura de tipos por id");
		
		return tipoRepository.findById(idTipo);
	}
	
	@Transactional(readOnly = true)
	public Optional<Tipo> obtenerTipoPorNombre(String nombre) {
		LOGGER.trace("Accediendo a la lectura de tipos por nombre");
		
		return tipoRepository.findByNombre(nombre);
	}
	
	public Tipo crearTiponuevo(Tipo tipo) {
		LOGGER.trace("Creando un tipo nuevo");
		
		return tipoRepository.save(tipo);
	}
	
	public Tipo actualizarElTipo(long id, Tipo tipo) {
		LOGGER.trace("Actualizado el tipo");
		
		Optional<Tipo> tipoParaActualizar = tipoRepository.findById(id);
		
		if(tipoParaActualizar.isEmpty()) {
			throw new TipoException("No se encontro ningún tipo para editar");
		}
		
		tipoParaActualizar.get().setNombre(tipo.getNombre());
		tipoParaActualizar.get().setDescripcion(tipo.getDescripcion());
		
		return tipoRepository.save(tipoParaActualizar.get());
	}
	
	public void eliminarTipo(long idTipo) {
		LOGGER.trace("Eliminando el tipo");
		
		tipoRepository.deleteById(idTipo);
	}
	
	public long getNumPaginas(int size) {
		
		long cantidadDeTipos = tipoRepository.count();
		
		long mod = cantidadDeTipos % size;
		
		if (mod > 0) {
			mod = 1;
		}
		return (cantidadDeTipos / size) + mod;
	}

}
