package es.cic.curso01.finalindividual01b.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import es.cic.curso01.finalindividual01b.models.Grupo;
import es.cic.curso01.finalindividual01b.models.GrupoVehiculo;
import es.cic.curso01.finalindividual01b.models.Vehiculo;

@Repository
public interface IGrupoVehiculoRepository extends JpaRepository<GrupoVehiculo, Long>{

	@Query("SELECT gv.vehiculo FROM GrupoVehiculo gv INNER JOIN Vehiculo v on v.id = gv.vehiculo WHERE gv.grupo = :grupo")
	public Page<Vehiculo> findVehiculoByGrupo(@Param("grupo")Grupo grupo, Pageable pageable);
	
	@Query("SELECT gv.grupo FROM GrupoVehiculo gv INNER JOIN Grupo g on g.id = gv.grupo WHERE gv.vehiculo = :vehiculo")
	public Page<Grupo> findByGrupoByVehiculo(@Param("vehiculo")Vehiculo vehiculo, Pageable pageable);
	
	public List<Optional<GrupoVehiculo>> findByGrupo(Grupo grupo);
}
