package es.cic.curso01.finalindividual01b.repositories;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import es.cic.curso01.finalindividual01b.models.Vehiculo;

@Repository
public interface IVehiculoRepository extends JpaRepository<Vehiculo, Long>{

	@Query("SELECT v FROM Vehiculo v INNER JOIN Marca m ON v.marca = m.id WHERE m.nombre = :marca")
	public Page<Vehiculo> findAllByMarca(@Param("marca")String marca, Pageable pageable);
	
	@Query("SELECT v FROM Vehiculo v INNER JOIN Tipo t ON v.tipo = t.id WHERE t.nombre = :tipo")
	public Page<Vehiculo> findAllByTipo(@Param("tipo")String tipo, Pageable pageable);
	
	public Optional<Vehiculo> findByModelo(String modelo);
}
