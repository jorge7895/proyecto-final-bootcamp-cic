package es.cic.curso01.finalindividual01b.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import es.cic.curso01.finalindividual01b.models.Grupo;

@Repository
public interface IGrupoRepository extends JpaRepository<Grupo, Long>{

	public Optional<Grupo> findByNombre(String nombre);
}
