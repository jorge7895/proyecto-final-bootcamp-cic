package es.cic.curso01.finalindividual01b.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import es.cic.curso01.finalindividual01b.models.Marca;

@Repository
public interface IMarcaRepository  extends JpaRepository<Marca, Long>{

	public Optional<Marca> findByNombre(String nombre);
}
