package es.cic.curso01.finalindividual01b.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import es.cic.curso01.finalindividual01b.models.Tipo;

@Repository
public interface ITipoRepository extends JpaRepository<Tipo, Long>{
	
	public Optional<Tipo> findByNombre(String nombre);

}
