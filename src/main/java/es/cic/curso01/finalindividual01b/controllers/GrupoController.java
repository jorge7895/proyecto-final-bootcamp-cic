package es.cic.curso01.finalindividual01b.controllers;

import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import es.cic.curso01.finalindividual01b.models.Grupo;
import es.cic.curso01.finalindividual01b.services.GrupoService;

@RestController
@RequestMapping(path = "/api/v1/grupo")
public class GrupoController {

	private static final Logger LOGGER = LogManager.getLogger(GrupoController.class);
	
	@Autowired
	private GrupoService grupoService;
	
	@GetMapping
	public ResponseEntity<List<Grupo>> obtenerTodosLosGrupos(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "3") int size){
		
		Pageable pageable = PageRequest.of(page, size);
		
		LOGGER.trace("Accediendo al controlador de obtencion de Grupos disponibles");
		
		List<Grupo> grupos = grupoService.obtenerTodosLosGrupos(pageable).getContent();
		
		return ResponseEntity.status(HttpStatus.ACCEPTED).contentType(MediaType.APPLICATION_JSON).body(grupos );
	}
	
	@GetMapping("/paginas")
	public ResponseEntity<Long> getNumPaginas(@RequestParam(name = "size") int size) {
		LOGGER.error("Accediendo al controlador de el número de páginas para un tamaño de página {0}", size);
		
		long numPaginas = grupoService.getNumPaginas(size);
		
		return ResponseEntity.status(HttpStatus.ACCEPTED).contentType(MediaType.APPLICATION_JSON).body(numPaginas);
	}
	
	@GetMapping("/id")
	public ResponseEntity<Optional<Grupo>> obtenerGrupoPorId(@RequestParam(name = "id") long id){
		
		LOGGER.error("Accediendo al controlador de obtencion de Grupo por id");
		
		
		Optional<Grupo> grupo = grupoService.obtenerGrupoPorId(id);
		
		return ResponseEntity.status(HttpStatus.ACCEPTED).contentType(MediaType.APPLICATION_JSON).body(grupo);
	}
	
	@GetMapping("/nombre")
	public ResponseEntity<Optional<Grupo>> obtenerGrupoPorNombre(@RequestParam(name = "nombre") String nombre){
		
		LOGGER.error("Accediendo al controlador de obtencion de Grupo por nombre");
		
		
		Optional<Grupo> grupo = grupoService.obtenerGrupoPorNombre(nombre);
		
		return ResponseEntity.status(HttpStatus.ACCEPTED).contentType(MediaType.APPLICATION_JSON).body(grupo);
	}
	
	@PostMapping
	public ResponseEntity<Grupo> crearGrupo(@Validated @RequestBody Grupo grupo) {
		LOGGER.trace("Accediendo al controlador de creación de un Grupo nuevo");
		
		Grupo grupoGuardado = grupoService.crearGruponuevo(grupo);
		
		return ResponseEntity
				.status(HttpStatus.CREATED)
				.contentType(MediaType.APPLICATION_JSON)
				.body(grupoGuardado);
	}
	
	@PutMapping("/modificar")
	public ResponseEntity<Grupo> actualizarGrupo(@RequestParam long id, @Validated @RequestBody Grupo grupo) {

		LOGGER.trace("Accediendo al controlador de modificación de un Grupo");

		
		Grupo grupoModificado = grupoService.actualizarElGrupo(id, grupo);

		return ResponseEntity.status(HttpStatus.ACCEPTED).contentType(MediaType.APPLICATION_JSON).body(grupoModificado);

	}
	
	@DeleteMapping("/eliminar")
	@ResponseStatus(code = HttpStatus.ACCEPTED)
	public void eliminarGrupo(@RequestParam long id) {

		LOGGER.trace("Accediendo al controlador de eliminación de un Grupo");

		grupoService.eliminarGrupo(id);
	}
}
