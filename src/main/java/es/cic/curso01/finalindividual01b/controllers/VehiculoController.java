package es.cic.curso01.finalindividual01b.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import es.cic.curso01.finalindividual01b.models.Vehiculo;
import es.cic.curso01.finalindividual01b.models.dto.VehiculoDto;
import es.cic.curso01.finalindividual01b.services.VehiculoService;
import es.cic.curso01.finalindividual01b.utils.Entidad2Dto;

@RestController
@RequestMapping(path = "/api/v1/vehiculo")
public class VehiculoController {

	private static final Logger LOGGER = LogManager.getLogger(VehiculoController.class);
	
	@Autowired
	private VehiculoService vehiculoService;
	
	private Entidad2Dto entidad2Dto = new Entidad2Dto();
	
	@GetMapping
	public ResponseEntity<List<VehiculoDto>> obtenerTodosLosVehiculos(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "3") int size){
		
		Pageable pageable = PageRequest.of(page, size);
		
		LOGGER.trace("Accediendo al controlador de obtencion de vehiculos disponibles");
		
		List<Vehiculo> vehiculos = vehiculoService.obtenerTodosLosVehiculos(pageable).getContent();
		List<VehiculoDto> vehiculosDto = new ArrayList<>();
		
		if(!vehiculos.isEmpty()) {
			vehiculos.stream().forEach(v -> vehiculosDto.add(entidad2Dto.vehiculo2VehiculoDto(v)));
		}
		return ResponseEntity.status(HttpStatus.ACCEPTED).contentType(MediaType.APPLICATION_JSON).body(vehiculosDto);
	}
	
	@GetMapping("/paginas")
	public ResponseEntity<Long> getNumPaginas(@RequestParam(name = "size") int size) {
		LOGGER.error("Accediendo al controlador de el número de páginas para un tamaño de página {0}", size);
		
		long numPaginas = vehiculoService.getNumPaginas(size);
		
		return ResponseEntity.status(HttpStatus.ACCEPTED).contentType(MediaType.APPLICATION_JSON).body(numPaginas);
	}
	
	@GetMapping("/id")
	public ResponseEntity<VehiculoDto> obtenerVehiculoPorId(@RequestParam(name = "id") long id){
		
		LOGGER.error("Accediendo al controlador de obtencion de vehiculo por id");
		
		Optional<Vehiculo> vehiculo = vehiculoService.obtenerVehiculoPorId(id);
		VehiculoDto dto = new VehiculoDto();
		
		if(!vehiculo.isEmpty()) {
			dto = entidad2Dto.vehiculo2VehiculoDto(vehiculo.get());
		}
		return ResponseEntity.status(HttpStatus.ACCEPTED).contentType(MediaType.APPLICATION_JSON).body(dto);
	}
	
	@GetMapping("/modelo")
	public ResponseEntity<Optional<Vehiculo>> obtenerVehiculoPorModelo(@RequestParam(name = "modelo") String modelo){
		
		LOGGER.error("Accediendo al controlador de obtencion de Vehiculos por modelo");
		
		
		Optional<Vehiculo> vehiculo = vehiculoService.obtenerVehiculoPorModelo(modelo);
		
		return ResponseEntity.status(HttpStatus.ACCEPTED).contentType(MediaType.APPLICATION_JSON).body(vehiculo);
	}
	
	@GetMapping("/marca")
	public ResponseEntity<List<VehiculoDto>> obtenerVehiculosPorMarca(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "3") int size, @RequestParam(name = "marca") String marca){
		
		LOGGER.error("Accediendo al controlador de obtencion de Vehiculos por marca");
		
		Pageable pageable = PageRequest.of(page, size);
		
		List<Vehiculo> vehiculos = vehiculoService.obtenerTodosLosVehiculosDeUnaMarca(pageable, marca).getContent();
		
		List<VehiculoDto> vehiculosDto = new ArrayList<>();
		
		if(!vehiculos.isEmpty()) {
			vehiculos.stream().forEach(v -> vehiculosDto.add(entidad2Dto.vehiculo2VehiculoDto(v)));
		}
		return ResponseEntity.status(HttpStatus.ACCEPTED).contentType(MediaType.APPLICATION_JSON).body(vehiculosDto);
	}
	
	@GetMapping("/tipo")
	public ResponseEntity<List<VehiculoDto>> obtenerVehiculosPorTipo(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "3") int size, @RequestParam(name = "tipo") String tipo){
		
		LOGGER.error("Accediendo al controlador de obtencion de Vehiculos por tipo");
		
		Pageable pageable = PageRequest.of(page, size);
		
		List<Vehiculo> vehiculos = vehiculoService.obtenerTodosLosVehiculosDeUnTipo(pageable, tipo).getContent();
		
		List<VehiculoDto> vehiculosDto = new ArrayList<>();
		
		if(!vehiculos.isEmpty()) {
			vehiculos.stream().forEach(v -> vehiculosDto.add(entidad2Dto.vehiculo2VehiculoDto(v)));
		}
		
		return ResponseEntity.status(HttpStatus.ACCEPTED).contentType(MediaType.APPLICATION_JSON).body(vehiculosDto);
	}
	
	@PostMapping
	public ResponseEntity<Vehiculo> crearVehiculo(@Validated @RequestBody Vehiculo vehiculo) {
		LOGGER.trace("Accediendo al controlador de creación de un Vehiculo nuevo");
		
		Vehiculo vehiculoGuardado = vehiculoService.crearVehiculonuevo(vehiculo);
		
		return ResponseEntity
				.status(HttpStatus.CREATED)
				.contentType(MediaType.APPLICATION_JSON)
				.body(vehiculoGuardado);
	}
	
	@PutMapping("/modificar")
	public ResponseEntity<Vehiculo> actualizarVehiculo(@RequestParam long id, @Validated @RequestBody Vehiculo vehiculo) {

		LOGGER.trace("Accediendo al controlador de modificación de un Vehiculo");

		
		Vehiculo vehiculoModificado = vehiculoService.actualizarElVehiculo(id, vehiculo);

		return ResponseEntity.status(HttpStatus.ACCEPTED).contentType(MediaType.APPLICATION_JSON).body(vehiculoModificado);

	}
	
	@DeleteMapping("/eliminar")
	@ResponseStatus(code = HttpStatus.ACCEPTED)
	public void eliminarVehiculo(@RequestParam long id) {

		LOGGER.trace("Accediendo al controlador de eliminación de un Vehiculo");

		vehiculoService.eliminarVehiculo(id);
	}
}
