package es.cic.curso01.finalindividual01b.controllers;

import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import es.cic.curso01.finalindividual01b.models.Tipo;
import es.cic.curso01.finalindividual01b.services.TipoService;

@RestController
@RequestMapping(path = "/api/v1/tipo")
public class TipoController {

	private static final Logger LOGGER = LogManager.getLogger(TipoController.class);
	
	@Autowired
	private TipoService tipoService;
	
	@GetMapping
	public ResponseEntity<List<Tipo>> obtenerTodosLosTipos(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "3") int size){
		
		Pageable pageable = PageRequest.of(page, size);
		
		LOGGER.trace("Accediendo al controlador de obtencion de tipos disponibles");
		
		List<Tipo> tipos = tipoService.obtenerTodosLosTipos(pageable).getContent();
		
		return ResponseEntity.status(HttpStatus.ACCEPTED).contentType(MediaType.APPLICATION_JSON).body(tipos);
	}
	
	@GetMapping("/id")
	public ResponseEntity<Optional<Tipo>> obtenerTipoPorId(@RequestParam(name = "id") long id){
		
		LOGGER.error("Accediendo al controlador de obtencion de tipo por id");
		
		
		Optional<Tipo> tipo = tipoService.obtenerTipoPorId(id);
		
		return ResponseEntity.status(HttpStatus.ACCEPTED).contentType(MediaType.APPLICATION_JSON).body(tipo);
	}
	
	@GetMapping("/paginas")
	public ResponseEntity<Long> getNumPaginas(@RequestParam(name = "size") int size) {
		LOGGER.error("Accediendo al controlador de el número de páginas para un tamaño de página {0}", size);
		
		long numPaginas = tipoService.getNumPaginas(size);
		
		return ResponseEntity.status(HttpStatus.ACCEPTED).contentType(MediaType.APPLICATION_JSON).body(numPaginas);
	}

	
	@GetMapping("/nombre")
	public ResponseEntity<Optional<Tipo>> obtenerTipoPorNombre(@RequestParam(name = "nombre") String nombre){
		
		LOGGER.error("Accediendo al controlador de obtencion de tipo por nombre");
		
		
		Optional<Tipo> tipo = tipoService.obtenerTipoPorNombre(nombre);
		
		return ResponseEntity.status(HttpStatus.ACCEPTED).contentType(MediaType.APPLICATION_JSON).body(tipo);
	}
	
	@PostMapping
	public ResponseEntity<Tipo> crearTipo(@Validated @RequestBody Tipo tipo) {
		LOGGER.trace("Accediendo al controlador de creación de un tipo nuevo");
		
		Tipo tipoGuardado = tipoService.crearTiponuevo(tipo);
		
		return ResponseEntity
				.status(HttpStatus.CREATED)
				.contentType(MediaType.APPLICATION_JSON)
				.body(tipoGuardado);
	}
	
	@PutMapping("/modificar")
	public ResponseEntity<Tipo> actualizarTipo(@RequestParam(name = "id") long id, @Validated @RequestBody Tipo tipo) {

		LOGGER.trace("Accediendo al controlador de modificación de un tipo");

		
		Tipo tipoModificado = tipoService.actualizarElTipo(id, tipo);

		return ResponseEntity.status(HttpStatus.ACCEPTED).contentType(MediaType.APPLICATION_JSON).body(tipoModificado);

	}
	
	@DeleteMapping("/eliminar")
	@ResponseStatus(code = HttpStatus.ACCEPTED)
	public void eliminarTipo(@RequestParam(name = "id") long id) {

		LOGGER.trace("Accediendo al controlador de eliminación de un tipo");

		tipoService.eliminarTipo(id);
	}
}
