package es.cic.curso01.finalindividual01b.controllers;

import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import es.cic.curso01.finalindividual01b.models.Marca;
import es.cic.curso01.finalindividual01b.services.MarcaService;

@RestController
@RequestMapping(path = "/api/v1/marca")
public class MarcaController {
	
	private static final Logger LOGGER = LogManager.getLogger(MarcaController.class);
	
	@Autowired
	private MarcaService marcaService;
	
	@GetMapping
	public ResponseEntity<List<Marca>> obtenerTodasLasMarcas(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "3") int size){
		
		Pageable pageable = PageRequest.of(page, size);
		
		LOGGER.trace("Accediendo al controlador de obtencion de marcas disponibles");
		
		List<Marca> marcas = marcaService.obtenerTodasLasMarcas(pageable).getContent();
		
		return ResponseEntity.status(HttpStatus.ACCEPTED).contentType(MediaType.APPLICATION_JSON).body(marcas);
	}
	
	@GetMapping("/paginas")
	public ResponseEntity<Long> getNumPaginas(@RequestParam(name = "size") int size) {
		LOGGER.error("Accediendo al controlador de el número de páginas para un tamaño de página {0}", size);
		
		long numPaginas = marcaService.getNumPaginas(size);
		
		return ResponseEntity.status(HttpStatus.ACCEPTED).contentType(MediaType.APPLICATION_JSON).body(numPaginas);
	}


	@GetMapping("/id")
	public ResponseEntity<Optional<Marca>> obtenerMarcaPorId(@RequestParam(name = "id") long id){
		
		LOGGER.error("Accediendo al controlador de obtencion de una marca por id");
		
		
		Optional<Marca> marca = marcaService.obtenerMarcaPorId(id);
		
		return ResponseEntity.status(HttpStatus.ACCEPTED).contentType(MediaType.APPLICATION_JSON).body(marca);
	}
	
	@GetMapping("/nombre")
	public ResponseEntity<Optional<Marca>> obtenerMarcaPorNombre(@RequestParam(name = "nombre") String nombre){
		
		LOGGER.error("Accediendo al controlador de obtencion de una marca por nombre");
		
		
		Optional<Marca> marca = marcaService.obtenerMarcaPorNombre(nombre);
		
		return ResponseEntity.status(HttpStatus.ACCEPTED).contentType(MediaType.APPLICATION_JSON).body(marca);
	}
	
	@PostMapping
	public ResponseEntity<Marca> crearTipo(@Validated @RequestBody Marca marca) {
		LOGGER.trace("Accediendo al controlador de creación de una marca nueva");
		
		Marca marcaGuardada = marcaService.crearMarcanueva(marca);
		
		return ResponseEntity
				.status(HttpStatus.CREATED)
				.contentType(MediaType.APPLICATION_JSON)
				.body(marcaGuardada);
	}
	
	@PutMapping("/modificar")
	public ResponseEntity<Marca> actualizarMarca(@RequestParam long id, @Validated @RequestBody Marca marca) {

		LOGGER.trace("Accediendo al controlador de modificación de una marca");

		
		Marca marcaModificada = marcaService.actualizarLaMarca(id, marca);

		return ResponseEntity.status(HttpStatus.ACCEPTED).contentType(MediaType.APPLICATION_JSON).body(marcaModificada);

	}
	
	@DeleteMapping("/eliminar")
	@ResponseStatus(code = HttpStatus.ACCEPTED)
	public void eliminarMarca(@RequestParam long id) {

		LOGGER.trace("Accediendo al controlador de eliminación de una marca");

		marcaService.eliminarMarca(id);
	}
}
