package es.cic.curso01.finalindividual01b.controllers;

import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import es.cic.curso01.finalindividual01b.models.Grupo;
import es.cic.curso01.finalindividual01b.models.GrupoVehiculo;
import es.cic.curso01.finalindividual01b.models.Vehiculo;
import es.cic.curso01.finalindividual01b.services.GrupoVehiculoService;

@RestController
@RequestMapping(path = "/api/v1/grupovehiculo")
public class GrupoVehiculoController {

	private static final Logger LOGGER = LogManager.getLogger(GrupoVehiculoController.class);
	
	@Autowired
	private GrupoVehiculoService grupoVehiculoService;
	
	@GetMapping
	public ResponseEntity<List<GrupoVehiculo>> obtenerTodosLosGrupoVehiculo(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "3") int size){
		
		Pageable pageable = PageRequest.of(page, size);
		
		LOGGER.trace("Accediendo al controlador de obtencion de Grupo Vehiculo disponibles");
		
		List<GrupoVehiculo> grupoVehiculos = grupoVehiculoService.obtenerTodosLosGrupoVehiculo(pageable).getContent();
		
		return ResponseEntity.status(HttpStatus.ACCEPTED).contentType(MediaType.APPLICATION_JSON).body(grupoVehiculos);
	}
	
	@GetMapping("/paginas")
	public ResponseEntity<Long> getNumPaginas(@RequestParam(name = "size") int size) {
		LOGGER.error("Accediendo al controlador de el número de páginas para un tamaño de página {0}", size);
		
		long numPaginas = grupoVehiculoService.getNumPaginas(size);
		
		return ResponseEntity.status(HttpStatus.ACCEPTED).contentType(MediaType.APPLICATION_JSON).body(numPaginas);
	}
	
	@GetMapping("/id")
	public ResponseEntity<Optional<GrupoVehiculo>> obtenerGrupoVehiculoPorId(@RequestParam(name = "id") long id){
		
		LOGGER.error("Accediendo al controlador de obtencion de Grupo Vehiculo por id");
		
		
		Optional<GrupoVehiculo> grupoVehiculo = grupoVehiculoService.obtenerGrupoVehiculoPorId(id);
		
		return ResponseEntity.status(HttpStatus.ACCEPTED).contentType(MediaType.APPLICATION_JSON).body(grupoVehiculo);
	}
	
	@GetMapping("/listapor/grupo")
	public ResponseEntity<List<Optional<GrupoVehiculo>>> grupoVehiculoPorGrupo(@RequestParam(name = "grupo") long id){
		
		
		LOGGER.error("Accediendo al controlador de obtencion de Vehiculos por grupo");
		
		
		List<Optional<GrupoVehiculo>> vehiculosDeUnGrupo = grupoVehiculoService.obtenerGrupoVehiculoPorGrupo(id);
		
		return ResponseEntity.status(HttpStatus.ACCEPTED).contentType(MediaType.APPLICATION_JSON).body(vehiculosDeUnGrupo);
	}
	
	@GetMapping("/grupo")
	public ResponseEntity<List<Vehiculo>> obtenerVehiculosDeGrupo(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "3") int size, @RequestParam(name = "grupo") long id){
		
		Pageable pageable = PageRequest.of(page, size);
		LOGGER.error("Accediendo al controlador de obtencion de Vehiculos por grupo");
		
		
		List<Vehiculo> vehiculosDeUnGrupo = grupoVehiculoService.obtenerVehiculosDeGrupo(id, pageable).getContent();
		
		return ResponseEntity.status(HttpStatus.ACCEPTED).contentType(MediaType.APPLICATION_JSON).body(vehiculosDeUnGrupo);
	}
	
	@GetMapping("/vehiculo")
	public ResponseEntity<List<Grupo>> obtenerGruposDeVehiculo(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "3") int size, @RequestParam(name = "vehiculo") long id){
		
		Pageable pageable = PageRequest.of(page, size);
		LOGGER.error("Accediendo al controlador de obtencion grupos de un vehiculo");
		
		
		List<Grupo> grupoVehiculos = grupoVehiculoService.obtenerGruposDeVehiculo(id, pageable).getContent();
		
		return ResponseEntity.status(HttpStatus.ACCEPTED).contentType(MediaType.APPLICATION_JSON).body(grupoVehiculos);
	}
	
	@PostMapping
	public ResponseEntity<GrupoVehiculo> crearGrupoVehiculo(@Validated @RequestBody GrupoVehiculo grupoVehiculo) {
		LOGGER.trace("Accediendo al controlador de creación de un Grupo Vehiculo nuevo");
		
		GrupoVehiculo grupoVehiculoGuardado = grupoVehiculoService.crearGrupoVehiculonuevo(grupoVehiculo);
		
		return ResponseEntity.status(HttpStatus.CREATED).contentType(MediaType.APPLICATION_JSON).body(grupoVehiculoGuardado);
	}
	
	@PostMapping("/crearmultiple")
	public ResponseEntity<List<GrupoVehiculo>> crearGrupoVehiculo(@Validated @RequestBody List<GrupoVehiculo> grupoVehiculo) {
		LOGGER.trace("Accediendo al controlador de creación de un Grupo Vehiculo nuevo");
		
		List<GrupoVehiculo> grupoVehiculoGuardado = grupoVehiculoService.crearVariosGrupoVehiculonuevo(grupoVehiculo);
		
		return ResponseEntity.status(HttpStatus.CREATED).contentType(MediaType.APPLICATION_JSON).body(grupoVehiculoGuardado);
	}
	
	@PutMapping("/modificar")
	public ResponseEntity<GrupoVehiculo> actualizarGrupoVehiculo(@RequestParam long id, @Validated @RequestBody GrupoVehiculo grupoVehiculo) {

		LOGGER.trace("Accediendo al controlador de modificación de un Grupo Vehiculo");

		
		GrupoVehiculo grupoVehiculoModificado = grupoVehiculoService.actualizarElGrupoVehiculo(id, grupoVehiculo);

		return ResponseEntity.status(HttpStatus.ACCEPTED).contentType(MediaType.APPLICATION_JSON).body(grupoVehiculoModificado);

	}
	
	@DeleteMapping("/eliminar")
	@ResponseStatus(code = HttpStatus.ACCEPTED)
	public void eliminarGrupoVehiculo(@RequestParam long id) {

		LOGGER.trace("Accediendo al controlador de eliminación de un Grupo Vehiculo");

		grupoVehiculoService.eliminarGrupoVehiculo(id);
	}
	
	@PutMapping("/eliminarvarios")
	public void eliminarGrupoVehiculo(@Validated @RequestBody List<GrupoVehiculo> grupoVehiculo) {

		LOGGER.trace("Accediendo al controlador de eliminación de varios Grupo Vehiculo");

		grupoVehiculoService.eliminarVariosGrupoVehiculo(grupoVehiculo);
	}
}
