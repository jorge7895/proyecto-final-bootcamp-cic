package es.cic.curso01.finalindividual01b.models;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.cic.curso01.finalindividual01b.utils.TipoCombustible;
import es.cic.curso01.finalindividual01b.utils.TipoTraccion;

@Entity
public class Vehiculo {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Version
	private long version;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tipo_id", foreignKey = @ForeignKey(name = "fk_tipo_tipoId"))
	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	private Tipo tipo;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "marca_id", foreignKey = @ForeignKey(name = "fk_marca_marcaId"))
	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	private Marca marca;
		
	private String modelo;
	private String matricula;
	private double kilometros;
	private int anoFabricacion;
	private String descripcion;
	private int puertas;
	private int plazas;
	private TipoTraccion traccion;
	private TipoCombustible combustible;
	
	public TipoCombustible getCombustible() {
		return combustible;
	}
	public void setCombustible(TipoCombustible combustible) {
		this.combustible = combustible;
	}
	public long getVersion() {
		return version;
	}
	public void setVersion(long version) {
		this.version = version;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Tipo getTipo() {
		return tipo;
	}
	public void setTipo(Tipo tipo) {
		this.tipo = tipo;
	}
	public Marca getMarca() {
		return marca;
	}
	public void setMarca(Marca marca) {
		this.marca = marca;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	public double getKilometros() {
		return kilometros;
	}
	public void setKilometros(double kilometros) {
		this.kilometros = kilometros;
	}
	public int getAnoFabricacion() {
		return anoFabricacion;
	}
	public void setAnoFabricacion(int anoFabricacion) {
		this.anoFabricacion = anoFabricacion;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public int getPuertas() {
		return puertas;
	}
	public void setPuertas(int puertas) {
		this.puertas = puertas;
	}
	public int getPlazas() {
		return plazas;
	}
	public void setPlazas(int plazas) {
		this.plazas = plazas;
	}
	public TipoTraccion getTraccion() {
		return traccion;
	}
	public void setTraccion(TipoTraccion traccion) {
		this.traccion = traccion;
	}
	@Override
	public int hashCode() {
		return Objects.hash(id);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vehiculo other = (Vehiculo) obj;
		return id == other.id;
	}
	@Override
	public String toString() {
		return "Vehiculo [tipo=" + tipo + ", marca=" + marca + ", modelo=" + modelo + ", matricula=" + matricula
				+ ", kilometros=" + kilometros + ", anoFabricacion=" + anoFabricacion + ", descripcion=" + descripcion
				+ ", puertas=" + puertas + ", plazas=" + plazas + ", traccion=" + traccion + "]";
	}
}
