package es.cic.curso01.finalindividual01b.utils;

import es.cic.curso01.finalindividual01b.models.Vehiculo;
import es.cic.curso01.finalindividual01b.models.dto.VehiculoDto;

public class Entidad2Dto {
	
	public VehiculoDto vehiculo2VehiculoDto (Vehiculo vehiculo) {
		
		VehiculoDto dto = new VehiculoDto();
		
		dto.setId(vehiculo.getId());
		dto.setIdTipo(vehiculo.getTipo() != null ? vehiculo.getTipo().getId() : 0);
		dto.setIdMarca(vehiculo.getMarca() != null ? vehiculo.getMarca().getId() : 0);
		dto.setModelo(vehiculo.getModelo());
		dto.setMatricula(vehiculo.getMatricula());
		dto.setKilometros(vehiculo.getKilometros());
		dto.setAnoFabricacion(vehiculo.getAnoFabricacion());
		dto.setDescripcion(vehiculo.getDescripcion());
		dto.setPuertas(vehiculo.getPuertas());
		dto.setPlazas(vehiculo.getPlazas());
		dto.setTraccion(vehiculo.getTraccion() != null ? vehiculo.getTraccion().toString() : "No encontrada");
		dto.setCombustible(vehiculo.getCombustible() != null ? vehiculo.getCombustible().toString() : "No encontrada");
		
		return dto;
	}
}
