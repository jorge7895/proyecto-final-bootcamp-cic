package es.cic.curso01.finalindividual01b.utils;

public enum TipoTraccion {
	DELANTERA,
	TRASERA,
	TOTAL
}
