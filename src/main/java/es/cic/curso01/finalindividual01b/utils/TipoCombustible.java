package es.cic.curso01.finalindividual01b.utils;

public enum TipoCombustible {
	DIESEL,
	GASOLINA,
	HIBRIDO,
	ELECTRICO,
	PEDALES
}
