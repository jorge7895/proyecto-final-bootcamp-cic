FROM maven:3.8.6-eclipse-temurin-17-focal

RUN apt-get update \
    && apt-get upgrade -y 

COPY ./target/*.jar /usr/app/vehiculos.jar

WORKDIR /usr/app/

RUN sh -c 'touch vehiculos.jar'

EXPOSE 8080

ENTRYPOINT [ "java","-jar","vehiculos.jar" ]