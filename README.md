# EMPRESA DE LOGISTICA
---
Aplicación para el alquiler de vehículos
	

### DESCRIPCIÓN
---
Este es el proyecto final del bootcamp de CIC, se trata de una aplicación de alquiler de vehículos


## CASOS DE USO
---
		> Realizar mantenimientos de vehículos
		> Realizar mantenimientos del tipo de vehículos
		> Realizar mantenimientos de las marcas
        > Los vehículos pertenecen a grupos, un vehículo puede pertenecer a varios grupos
 
# DESARROLLADOR
---
	Jorge Pardo 
